from behave import *
from configuration.config import TestData
from pages.HomePage import HomePage
from pages.PdpPage import PdpPage


@given('launch chrome Browser ')
def launch_browser(context):
    print()


@when('open demoblaze site to add product')
def launch_demoblaze_site(context):
    context.web.open(TestData.URL)


@then('click on category phone and click the first listed phone')
def click_phone_cateory(context):
    try:
        context.homepage = HomePage(context.web._web_driver)
        # context.homepage.click_category_phone()
        context.homepage.click_first_item_in_phone()

    except:
        context.web._web_driver.close()
        assert False, "Test is failed in click category link"


@then('navigate to product description page and validate the contents')
def navigate_to_pdp(context):
    try:
        context.pdppage = PdpPage(context.web._web_driver)
        context.pdppage.validate_product_title()
        # context.pdppage.validate_product_description()
        context.pdppage.click_add_to_cart()
    except:
        context.web._web_driver.close()
        assert False, "pdp description"
