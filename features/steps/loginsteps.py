from behave import *
from selenium import webdriver

from configuration.config import TestData
from pages.HomePage import HomePage
from pages.LoginPage import LoginPage


@given('launch chrome Browser')
def launch_browser(context):
    print()


@when('open demoblaze site')
def launch_demoblaze_site(context):
    context.web.open(TestData.URL)


@then('click login button')
def demoblazehome(context):
    try:
        context.homepage = HomePage(context.web._web_driver)
        context.homepage.click_login_link()

    except:
        context.web._web_driver.close()
        assert False, "Test is failed in click login link"


@then(u'Provide the username "{user}" and password "{pwd}"')
def demoblazelogin(context, user, pwd):
    try:
        context.loginpage = LoginPage(context.web._web_driver)
        context.loginpage.enter_login_creds(user, pwd)
        context.loginpage.enter_login()

    except:
        context.web._web_driver.close()
        assert False, "Test is failed in launch login page section"

'''
@then(u'quit the browser')
def quitdriver(context):
        context.web._web_driver.quit()
'''