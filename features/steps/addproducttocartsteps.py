import time

from behave import *
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from configuration.config import TestData
from pages.CheckoutPage import CheckoutPage
from pages.HomePage import HomePage
from pages.PdpPage import PdpPage


@given('Launch demoblaze url')
def launch_browser(context):
    print()


@when('Search a product')
def launch_demoblaze_site(context):
    context.web.open(TestData.URL)


@then('Add the product to cart')
def click_phone_cateory(context):
    try:
        context.pdppage = PdpPage(context.web._web_driver)
        context.homepage = HomePage(context.web._web_driver)
        context.homepage.click_first_item_in_phone()

        context.pdppage.click_add_to_cart()
        WebDriverWait(context.web._web_driver, 10).until(EC.alert_is_present())
        context.web._web_driver.switch_to.alert.accept()
        context.homepage.click_cart_link()

    except:
        context.web._web_driver.close()
        assert False, "Test is failed in Adding product"


@then('purchase the product')
def purchase_the_product(context):
    try:

        context.checkoutpage = CheckoutPage(context.web._web_driver)
        context.checkoutpage.click_place_order_button()
        time.sleep(2)
        context.checkoutpage.enter_checkout_details_modal()

    except:
        context.web._web_driver.close()
        assert False, "Checkout failed"
