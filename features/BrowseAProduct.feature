Feature: Browse a product in demoblaze application
  As an online shopper,
  i want to browse products based on category
  and read thru the descriptions

  Scenario: Open demoblaze website and browse a product
    Given launch chrome Browser
    When open demoblaze site to add product
    Then click on category phone and click the first listed phone
    Then navigate to product description page and validate the contents

