Feature: Add items to cart and purchase the items
  As an online shopper,
  I want search a product,
  and purchase the product flawlessly


  Scenario: Add product to the cart
    Given Launch demoblaze url
    When Search a product
    Then Add the product to cart
    Then purchase the product

