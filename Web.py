from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class Web(object):
    __TIMEOUT = 10

    def __init__(self, web_driver):
        super(Web, self).__init__()  # in python 3.6 you can just call super().__init__()

        self._web_driver_wait = WebDriverWait(web_driver, Web.__TIMEOUT)
        self._web_driver = web_driver

    def open(self, url):
        self._web_driver.get(url)

    def wait_for_element_visibility(self, xpath):
        return self._web_driver_wait.until(EC.visibility_of_element_located((By.XPATH, xpath)))

    def wait_for_elements_visibility(self, xpath):
        return self._web_driver_wait.until(EC.presence_of_all_elements_located((By.XPATH, xpath)))

    def wait_for_elements_clickable(self, xpath):
        return self._web_driver_wait.until(EC.element_to_be_clickable(By.XPATH, xpath))

    def getTitle(self):
        return self._web_driver.title

    def quitdriver(self):
        return self._web_driver.quit()

