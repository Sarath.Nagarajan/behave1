from selenium import webdriver

from Web import Web


def browser_chrome(context, timeout=30, **kwargs):
    options = webdriver.ChromeOptions()
    options.add_argument('--start-maximized')
    options.add_argument('--no-sandbox')
    browser = webdriver.Chrome(
        r"C:\Users\132654\OneDrive - Cognizant\Documents\Drivers\chromedriver_win32\chromedriver.exe",
        chrome_options=options)
    web = Web(browser)
    context.web = web
    yield context.web
    browser.stop_client()
    browser.quit()
    print(context.scenario.status)
