from selenium.webdriver.common.by import By

from pages.BasePage import BasePage


class PdpPage(BasePage):
    TXT_PRODUCT_TITLE = (By.XPATH, "//h2[contains(text(),'Samsung')]")
    TXT_PRODUCT_DESCRIPTION = (By.XPATH, "//p[contains(text(),'Samsung ')]")
    BTN_ADD_TO_CART = (By.XPATH, "//*[contains(@class,'btn btn-success btn-lg')]")

    def __int__(self, driver):
        super().__init__(driver)

    def validate_product_title(self):
        assert self.get_element_text(self.TXT_PRODUCT_TITLE) == "Samsung galaxy s6"

    def validate_product_description(self):
        assert self.get_element_text(
            self.TXT_PRODUCT_DESCRIPTION) == "The Samsung Galaxy S6 is powered by 1.5GHz octa-core Samsung Exynos " \
                                             "7420 processor and it comes with 3GB of RAM. The phone packs 32GB of " \
                                             "internal storage cannot be expanded. "

    def click_add_to_cart(self):
        self.click_element(self.BTN_ADD_TO_CART)