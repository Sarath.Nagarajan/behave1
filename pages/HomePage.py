from time import sleep

from selenium.webdriver.common.by import By

from pages.BasePage import BasePage


class HomePage(BasePage):
    LINK_LOGIN = (By.ID, "login2")
    LINK_HOME = (By.LINK_TEXT, "Home")
    VALIDATE_LOGIN = (By.ID, "nameofuser")
    LINK_LOGOUT = (By.ID, "logout2")
    LINK_PHONE = (By.XPATH, "//a[contains(text(),'Phones')]")
    LINK_FIRST_ITEM = (By.XPATH, "//a[contains(text(),'Samsung galaxy s6')]")
    LINK_CART = (By.ID, "cartur")

    """Constructor of CarrersPage class"""

    def __int__(self, driver):
        super().__init__(driver)

    def click_login_link(self):
        self.click_element(self.LINK_LOGIN)

    def click_category_phone(self):
        self.click_element(self.LINK_PHONE)

    def click_first_item_in_phone(self):
        self.click_element(self.LINK_FIRST_ITEM)

    def click_cart_link(self):
        self.click_element(self.LINK_CART)