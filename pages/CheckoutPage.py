from selenium.webdriver.common.by import By

from pages.BasePage import BasePage
from selenium.webdriver.common.alert import Alert


class CheckoutPage(BasePage):
    BTN_PLACE_ORDER = (By.XPATH, "//*[contains(@class,'btn btn-success')]")
    PRODUCT_COUNT = (By.XPATH, "//table/tbody/tr")

    #PLACE ORDER MODAL

    PLACE_ORDER_MODAL = (By.ID, "orderModalLabel")
    PLACE_ORDER_NAME = (By.ID, "name")
    PLACE_ORDER_COUNTRY = (By.ID, "country")
    PLACE_ORDER_CITY = (By.ID, "city")
    PLACE_ORDER_CREDIT_CARD = (By.ID, "card")
    PLACE_ORDER_CREDIT_CARD_MONTH = (By.ID, "month")
    PLACE_ORDER_CREDIT_CARD_YEAR = (By.ID, "year")
    BTN_PLACE_ORDER_PURCHASE = (By.XPATH, "//button[contains(text(),'Purchase')]")


    """Constructor of CarrersPage class"""

    def __int__(self, driver):
        super().__init__(driver)

    def check_number_of_products_in_cart(self):
        number_of_products = len(self.PRODUCT_COUNT)
        print('Number of Products:' + number_of_products)

    def click_place_order_button(self):
        self.click_element(self.BTN_PLACE_ORDER)

    def enter_checkout_details_modal(self):
        self.input_element(self.PLACE_ORDER_NAME, "test")
        self.input_element(self.PLACE_ORDER_COUNTRY, "United Kingdom")
        self.input_element(self.PLACE_ORDER_CITY, "Leeds")
        self.input_element(self.PLACE_ORDER_CREDIT_CARD, "4111111111111111")
        self.input_element(self.PLACE_ORDER_CREDIT_CARD_MONTH, "12")
        self.input_element(self.PLACE_ORDER_CREDIT_CARD_YEAR, "2024")
        self.click_element(self.BTN_PLACE_ORDER_PURCHASE)