from selenium.webdriver.common.by import By

from pages.BasePage import BasePage


class LoginPage(BasePage):
    TXT_USERNAME = (By.ID, "loginusername")
    TXT_PASSWORD = (By.ID, "loginpassword")
    BTN_LOGIN = (By.XPATH, "//button[contains(text(),'Log in')]")


    def __init__(self, driver):
        super().__init__(driver)

    def enter_login_creds(self, user, pwd):
        self.input_element(self.TXT_USERNAME, user)
        self.input_element(self.TXT_PASSWORD, pwd)

    def enter_username(self, user):
        self.input_element(self.TXT_USERNAME, user)

    def enter_password(self, pwd):
        self.input_element(self.TXT_PASSWORD, pwd)

    def enter_login(self):
        self.click_element(self.BTN_LOGIN)

